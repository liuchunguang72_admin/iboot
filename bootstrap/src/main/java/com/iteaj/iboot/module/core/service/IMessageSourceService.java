package com.iteaj.iboot.module.core.service;

import com.iteaj.iboot.module.core.entity.MessageSource;
import com.iteaj.framework.IBaseService;

/**
 * <p>
 * 消息源 服务类
 * </p>
 *
 * @author iteaj
 * @since 2023-07-30
 */
public interface IMessageSourceService extends IBaseService<MessageSource> {

}
